# Welcome

The _2nd Communication, Media and Journalism Research Group Conference_ aims to unite postgraduate researchers and early career academics whose research lies in the interconnected fields of communication, media and journalism. 

This one day online conference focuses on the broad theme of crisis. 
We are interested in engaging with the interrelationships between political, economic, healthcare and media industry crises, both past and present.

# Programme

_Please note that all timings are GMT._

Hosted by the Department of Journalism Studies at the University of Sheffield, 14 January 2021.

## Opening

[Join Session](https://meet.jit.si/cmjconf2021welcome)

- 11:00-11:15     Welcome talk by Professor Jackie Harrison       

## National and International Crises

### Panel One 

[Join Session](https://meet.jit.si/cmjconf2021natint1)

- 11:15-11:30     Sulayman Bah
- 11:30-11:45     Rosemarie Calleja
- 11:45-12:00     Umanga Perera
- 12:00-12:15     Dr Arif Hussain	Nadaf
- 12:15-12:30     Nayara H. C. Güércio and Victor L. Cruzeiro
- 12:30-12:45     Q&A Session

### Panel Two 
 
[Join Session](https://meet.jit.si/cmjconf2021natint2)

- 11:15-11:30     Santiago	Correa
- 11:30-11:45     Alessandra	Massa
- 11:45-12:00     Aspriadis	Neofytos
- 12:00-12:15     Ana	Matias	and	Ana	Marcelo
- 12:15-12:45     Q&A Session

## Lunch

12:45-13:15

## Crises in the Pandemic

### Panel One 

[Join Session](https://meet.jit.si/cmjconf2021covid1)

- 13:15-13:30     Normahfuzah	Ahmad
- 13:30-13:45     Sonia	Regina	Soares	Da Cunha and Maria	Érica de Oliveira Lima
- 13:45-14:15     Q&A Session

### Panel Two 
 
[Join Session](https://meet.jit.si/cmjconf2021covid2)

- 13:15-13:30     Manushi Mansi
- 13:30-13:45     Sandra	Simonsen
- 13:45-14:00     Sašo Slaček Brlek and Jernej Kaluža
- 14:00-14:15     Q&A Session

## Break

14:15-14:45

## Crises in Journalism

### Panel One 

[Join Session](https://meet.jit.si/cmjconf2021jnl1)

- 14:45-15:00     Dr Mélanie	Dupéré
- 15:00-15:15     Zhenghan	Gao
- 15:15-15:30     Ndawana	Youngson
- 15:30-15:45     Mahmoud Farhadimahali, Mohammad	Mojaver Sheikhan and Zohre Javadieh
- 15:45-16:00     Q&A Session

### Panel Two 
 
[Join Session](https://meet.jit.si/cmjconf2021jnl2)

- 14:45-15:00     Izabela	Korbiel
- 15:00-15:15     Hossein	Afkhami	and Elham	Shamoradi
- 15:15-15:30     Professor	Stephen	Cushion, Dr	Richard	Thomas and	Dr	Declan	McDowell-Naylor
- 15:30-16:00     Q&A Session     

## Closing Remarks

[Join Session](https://meet.jit.si/cmjconf2021close)
