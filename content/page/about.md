---
title: About CMJ
comments: false
---

### Communication, Media and Journalism Research Group

The [Communication, Media and Journalism research group (CMJ)](https://www.sheffield.ac.uk/journalism/research) is home to the research-active staff and students from the University of Sheffield’s Department of Journalism Studies, the Centre for Freedom of the Media (CFOM) and the Centre for the Study of Journalism and History. 
The CMJ is an interdisciplinary group with research strengths in data science, psychology, history, international relations, journalism, international law, media and communication, political science and sociology. 
Our researchers work across the broad areas of communication, media and journalism.
Our research themes include:

+ Conflict and crisis communication
+ European and comparative media and information law
+ Journalism, communication and democracy
+ Journalism and communication history
+ Language, communication and journalism
+ The digital media and communication environment

 

